const Sudoku = require('./../sudoku.js'),
	source = [
		[0, 4, 0,	0, 0, 0, 	0, 0, 0],
		[0, 0, 0, 	0, 7, 0, 	0, 0, 0],
		[0, 8, 0, 	0, 0, 0, 	6, 0, 0],
	
		[3, 6, 0, 	0, 0, 1, 	4, 2, 0],
		[0, 0, 0, 	2, 0, 0, 	5, 3, 0],
		[0, 0, 4, 	0, 0, 3, 	0, 6, 7],
	
		[7, 0, 0, 	0, 0, 8, 	0, 0, 0],
		[0, 1, 0, 	3, 0, 6, 	0, 5, 0],
		[6, 3, 0, 	0, 0, 0, 	0, 4, 0]
	],
	chai = require('chai'),
	expect = chai.expect;

describe('sudoku', () => {
	describe('this.', () => {
		describe('checkLineFor', () => {
			it('Expect to return if there is no n in the line x', () => {
				const sudoku = new Sudoku(source);
	
				// [0, 4, 0,	0, 0, 0, 	0, 0, 0],
				expect(sudoku.checkLineFor(0, 1)).to.be.true;
				expect(sudoku.checkLineFor(0, 2)).to.be.true;
				expect(sudoku.checkLineFor(0, 3)).to.be.true;
				expect(sudoku.checkLineFor(0, 4)).to.be.false;
				expect(sudoku.checkLineFor(0, 5)).to.be.true;
				expect(sudoku.checkLineFor(0, 6)).to.be.true;
				expect(sudoku.checkLineFor(0, 7)).to.be.true;
				expect(sudoku.checkLineFor(0, 8)).to.be.true;
				expect(sudoku.checkLineFor(0, 9)).to.be.true;
	
				// [0, 0, 0, 	0, 7, 0, 	0, 0, 0],
				expect(sudoku.checkLineFor(1, 1)).to.be.true;
				expect(sudoku.checkLineFor(1, 2)).to.be.true;
				expect(sudoku.checkLineFor(1, 3)).to.be.true;
				expect(sudoku.checkLineFor(1, 4)).to.be.true;
				expect(sudoku.checkLineFor(1, 5)).to.be.true;
				expect(sudoku.checkLineFor(1, 6)).to.be.true;
				expect(sudoku.checkLineFor(1, 7)).to.be.false;
				expect(sudoku.checkLineFor(1, 8)).to.be.true;
				expect(sudoku.checkLineFor(1, 9)).to.be.true;
	
				// [0, 8, 0, 	0, 0, 0, 	6, 0, 0],
				expect(sudoku.checkLineFor(2, 1)).to.be.true;
				expect(sudoku.checkLineFor(2, 2)).to.be.true;
				expect(sudoku.checkLineFor(2, 3)).to.be.true;
				expect(sudoku.checkLineFor(2, 4)).to.be.true;
				expect(sudoku.checkLineFor(2, 5)).to.be.true;
				expect(sudoku.checkLineFor(2, 6)).to.be.false;
				expect(sudoku.checkLineFor(2, 7)).to.be.true;
				expect(sudoku.checkLineFor(2, 8)).to.be.false;
				expect(sudoku.checkLineFor(2, 9)).to.be.true;
	
				// [3, 6, 0, 	0, 0, 1, 	4, 2, 0],
				expect(sudoku.checkLineFor(3, 1)).to.be.false;
				expect(sudoku.checkLineFor(3, 2)).to.be.false;
				expect(sudoku.checkLineFor(3, 3)).to.be.false;
				expect(sudoku.checkLineFor(3, 4)).to.be.false;
				expect(sudoku.checkLineFor(3, 5)).to.be.true;
				expect(sudoku.checkLineFor(3, 6)).to.be.false;
				expect(sudoku.checkLineFor(3, 7)).to.be.true;
				expect(sudoku.checkLineFor(3, 8)).to.be.true;
				expect(sudoku.checkLineFor(3, 9)).to.be.true;
	
				// [0, 0, 0, 	2, 0, 0, 	5, 3, 0],
				expect(sudoku.checkLineFor(4, 1)).to.be.true;
				expect(sudoku.checkLineFor(4, 2)).to.be.false;
				expect(sudoku.checkLineFor(4, 3)).to.be.false;
				expect(sudoku.checkLineFor(4, 4)).to.be.true;
				expect(sudoku.checkLineFor(4, 5)).to.be.false;
				expect(sudoku.checkLineFor(4, 6)).to.be.true;
				expect(sudoku.checkLineFor(4, 7)).to.be.true;
				expect(sudoku.checkLineFor(4, 8)).to.be.true;
				expect(sudoku.checkLineFor(4, 9)).to.be.true;
	
				// [0, 0, 4, 	0, 0, 3, 	0, 6, 7],
				expect(sudoku.checkLineFor(5, 1)).to.be.true;
				expect(sudoku.checkLineFor(5, 2)).to.be.true;
				expect(sudoku.checkLineFor(5, 3)).to.be.false;
				expect(sudoku.checkLineFor(5, 4)).to.be.false;
				expect(sudoku.checkLineFor(5, 5)).to.be.true;
				expect(sudoku.checkLineFor(5, 6)).to.be.false;
				expect(sudoku.checkLineFor(5, 7)).to.be.false;
				expect(sudoku.checkLineFor(5, 8)).to.be.true;
				expect(sudoku.checkLineFor(5, 9)).to.be.true;
			
				// [7, 0, 0, 	0, 0, 8, 	0, 0, 0],
				expect(sudoku.checkLineFor(6, 1)).to.be.true;
				expect(sudoku.checkLineFor(6, 2)).to.be.true;
				expect(sudoku.checkLineFor(6, 3)).to.be.true;
				expect(sudoku.checkLineFor(6, 4)).to.be.true;
				expect(sudoku.checkLineFor(6, 5)).to.be.true;
				expect(sudoku.checkLineFor(6, 6)).to.be.true;
				expect(sudoku.checkLineFor(6, 7)).to.be.false;
				expect(sudoku.checkLineFor(6, 8)).to.be.false;
				expect(sudoku.checkLineFor(6, 9)).to.be.true;
	
				// [0, 1, 0, 	3, 0, 6, 	0, 5, 0],
				expect(sudoku.checkLineFor(7, 1)).to.be.false;
				expect(sudoku.checkLineFor(7, 2)).to.be.true;
				expect(sudoku.checkLineFor(7, 3)).to.be.false;
				expect(sudoku.checkLineFor(7, 4)).to.be.true;
				expect(sudoku.checkLineFor(7, 5)).to.be.false;
				expect(sudoku.checkLineFor(7, 6)).to.be.false;
				expect(sudoku.checkLineFor(7, 7)).to.be.true;
				expect(sudoku.checkLineFor(7, 8)).to.be.true;
				expect(sudoku.checkLineFor(7, 9)).to.be.true;
	
				// [6, 3, 0, 	0, 0, 0, 	0, 4, 0]
				expect(sudoku.checkLineFor(8, 1)).to.be.true;
				expect(sudoku.checkLineFor(8, 2)).to.be.true;
				expect(sudoku.checkLineFor(8, 3)).to.be.false;
				expect(sudoku.checkLineFor(8, 4)).to.be.false;
				expect(sudoku.checkLineFor(8, 5)).to.be.true;
				expect(sudoku.checkLineFor(8, 6)).to.be.false;
				expect(sudoku.checkLineFor(8, 7)).to.be.true;
				expect(sudoku.checkLineFor(8, 8)).to.be.true;
				expect(sudoku.checkLineFor(8, 9)).to.be.true;
			});
		});
	
		describe('checkColumnFor', () => {
			it('Expect to return if there is no n in the column y', () => {
				const sudoku = new Sudoku(source);
				
				// [0, 0, 0, 3, 0, 0, 7, 0, 6]
				expect(sudoku.checkColumnFor(0, 1)).to.be.true;
				expect(sudoku.checkColumnFor(0, 2)).to.be.true;
				expect(sudoku.checkColumnFor(0, 3)).to.be.false;
				expect(sudoku.checkColumnFor(0, 4)).to.be.true;
				expect(sudoku.checkColumnFor(0, 5)).to.be.true;
				expect(sudoku.checkColumnFor(0, 6)).to.be.false;
				expect(sudoku.checkColumnFor(0, 7)).to.be.false;
				expect(sudoku.checkColumnFor(0, 8)).to.be.true;
				expect(sudoku.checkColumnFor(0, 9)).to.be.true;
	
				// [4, 0, 8, 6, 0, 0, 0, 1, 3]
				expect(sudoku.checkColumnFor(1, 1)).to.be.false;
				expect(sudoku.checkColumnFor(1, 2)).to.be.true;
				expect(sudoku.checkColumnFor(1, 3)).to.be.false;
				expect(sudoku.checkColumnFor(1, 4)).to.be.false;
				expect(sudoku.checkColumnFor(1, 5)).to.be.true;
				expect(sudoku.checkColumnFor(1, 6)).to.be.false;
				expect(sudoku.checkColumnFor(1, 7)).to.be.true;
				expect(sudoku.checkColumnFor(1, 8)).to.be.false;
				expect(sudoku.checkColumnFor(1, 9)).to.be.true;
	
				// [0, 0, 0, 0, 0, 4, 0, 0, 0]
				expect(sudoku.checkColumnFor(2, 1)).to.be.true;
				expect(sudoku.checkColumnFor(2, 2)).to.be.true;
				expect(sudoku.checkColumnFor(2, 3)).to.be.true;
				expect(sudoku.checkColumnFor(2, 4)).to.be.false;
				expect(sudoku.checkColumnFor(2, 5)).to.be.true;
				expect(sudoku.checkColumnFor(2, 6)).to.be.true;
				expect(sudoku.checkColumnFor(2, 7)).to.be.true;
				expect(sudoku.checkColumnFor(2, 8)).to.be.true;
				expect(sudoku.checkColumnFor(2, 9)).to.be.true;
				
				// [0, 0, 0, 0, 2, 0, 0, 3, 0]
				expect(sudoku.checkColumnFor(3, 1)).to.be.true;
				expect(sudoku.checkColumnFor(3, 2)).to.be.false;
				expect(sudoku.checkColumnFor(3, 3)).to.be.false;
				expect(sudoku.checkColumnFor(3, 4)).to.be.true;
				expect(sudoku.checkColumnFor(3, 5)).to.be.true;
				expect(sudoku.checkColumnFor(3, 6)).to.be.true;
				expect(sudoku.checkColumnFor(3, 7)).to.be.true;
				expect(sudoku.checkColumnFor(3, 8)).to.be.true;
				expect(sudoku.checkColumnFor(3, 9)).to.be.true;
	
				// [0, 7, 0, 0, 0, 0, 0, 0, 0], 
				expect(sudoku.checkColumnFor(4, 1)).to.be.true;
				expect(sudoku.checkColumnFor(4, 2)).to.be.true;
				expect(sudoku.checkColumnFor(4, 3)).to.be.true;
				expect(sudoku.checkColumnFor(4, 4)).to.be.true;
				expect(sudoku.checkColumnFor(4, 5)).to.be.true;
				expect(sudoku.checkColumnFor(4, 6)).to.be.true;
				expect(sudoku.checkColumnFor(4, 7)).to.be.false;
				expect(sudoku.checkColumnFor(4, 8)).to.be.true;
				expect(sudoku.checkColumnFor(4, 9)).to.be.true;
	
				// [0, 0, 0, 1, 0, 3, 8, 6, 0], 
				expect(sudoku.checkColumnFor(5, 1)).to.be.false;
				expect(sudoku.checkColumnFor(5, 2)).to.be.true;
				expect(sudoku.checkColumnFor(5, 3)).to.be.false;
				expect(sudoku.checkColumnFor(5, 4)).to.be.true;
				expect(sudoku.checkColumnFor(5, 5)).to.be.true;
				expect(sudoku.checkColumnFor(5, 6)).to.be.false;
				expect(sudoku.checkColumnFor(5, 7)).to.be.true;
				expect(sudoku.checkColumnFor(5, 8)).to.be.false;
				expect(sudoku.checkColumnFor(5, 9)).to.be.true;
				
				// [0, 0, 6, 4, 5, 0, 0, 0, 0], 
				expect(sudoku.checkColumnFor(6, 1)).to.be.true;
				expect(sudoku.checkColumnFor(6, 2)).to.be.true;
				expect(sudoku.checkColumnFor(6, 3)).to.be.true;
				expect(sudoku.checkColumnFor(6, 4)).to.be.false;
				expect(sudoku.checkColumnFor(6, 5)).to.be.false;
				expect(sudoku.checkColumnFor(6, 6)).to.be.false;
				expect(sudoku.checkColumnFor(6, 7)).to.be.true;
				expect(sudoku.checkColumnFor(6, 8)).to.be.true;
				expect(sudoku.checkColumnFor(6, 9)).to.be.true;
	
				// [0, 0, 0, 2, 3, 6, 0, 5, 4], 
				expect(sudoku.checkColumnFor(7, 1)).to.be.true;
				expect(sudoku.checkColumnFor(7, 2)).to.be.false;
				expect(sudoku.checkColumnFor(7, 3)).to.be.false;
				expect(sudoku.checkColumnFor(7, 4)).to.be.false;
				expect(sudoku.checkColumnFor(7, 5)).to.be.false;
				expect(sudoku.checkColumnFor(7, 6)).to.be.false;
				expect(sudoku.checkColumnFor(7, 7)).to.be.true;
				expect(sudoku.checkColumnFor(7, 8)).to.be.true;
				expect(sudoku.checkColumnFor(7, 9)).to.be.true;
	
				// [0, 0, 0, 0, 0, 7, 0, 0, 0]
				expect(sudoku.checkColumnFor(8, 1)).to.be.true;
				expect(sudoku.checkColumnFor(8, 2)).to.be.true;
				expect(sudoku.checkColumnFor(8, 3)).to.be.true;
				expect(sudoku.checkColumnFor(8, 4)).to.be.true;
				expect(sudoku.checkColumnFor(8, 5)).to.be.true;
				expect(sudoku.checkColumnFor(8, 6)).to.be.true;
				expect(sudoku.checkColumnFor(8, 7)).to.be.false;
				expect(sudoku.checkColumnFor(8, 8)).to.be.true;
				expect(sudoku.checkColumnFor(8, 9)).to.be.true;
			});
		});
	
		describe('checkBlockFor', () => {
			it('Expect to return if there is no n in the bloc containing coordinates x/y', () => {
				const sudoku = new Sudoku(source);
	
				// [0, 4, 0, 0, 0, 0, 0, 8, 0]
				expect(sudoku.checkBlocFor(0, 0, 1)).to.be.true;
				expect(sudoku.checkBlocFor(0, 0, 2)).to.be.true;
				expect(sudoku.checkBlocFor(0, 0, 3)).to.be.true;
				expect(sudoku.checkBlocFor(0, 0, 4)).to.be.false;
				expect(sudoku.checkBlocFor(0, 0, 5)).to.be.true;
				expect(sudoku.checkBlocFor(0, 0, 6)).to.be.true;
				expect(sudoku.checkBlocFor(0, 0, 7)).to.be.true;
				expect(sudoku.checkBlocFor(0, 0, 8)).to.be.false;
				expect(sudoku.checkBlocFor(0, 0, 9)).to.be.true;
	
				// [0, 0, 0, 0, 7, 0, 0, 0, 0]
				expect(sudoku.checkBlocFor(0, 3, 1)).to.be.true;
				expect(sudoku.checkBlocFor(0, 3, 2)).to.be.true;
				expect(sudoku.checkBlocFor(0, 3, 3)).to.be.true;
				expect(sudoku.checkBlocFor(0, 3, 4)).to.be.true;
				expect(sudoku.checkBlocFor(0, 3, 5)).to.be.true;
				expect(sudoku.checkBlocFor(0, 3, 6)).to.be.true;
				expect(sudoku.checkBlocFor(0, 3, 7)).to.be.false;
				expect(sudoku.checkBlocFor(0, 3, 8)).to.be.true;
				expect(sudoku.checkBlocFor(0, 3, 9)).to.be.true;
	
				// [0, 0, 0, 0, 0, 0, 6, 0, 0]
				expect(sudoku.checkBlocFor(0, 6, 1)).to.be.true;
				expect(sudoku.checkBlocFor(0, 6, 2)).to.be.true;
				expect(sudoku.checkBlocFor(0, 6, 3)).to.be.true;
				expect(sudoku.checkBlocFor(0, 6, 4)).to.be.true;
				expect(sudoku.checkBlocFor(0, 6, 5)).to.be.true;
				expect(sudoku.checkBlocFor(0, 6, 6)).to.be.false;
				expect(sudoku.checkBlocFor(0, 6, 7)).to.be.true;
				expect(sudoku.checkBlocFor(0, 6, 8)).to.be.true;
				expect(sudoku.checkBlocFor(0, 6, 9)).to.be.true;
	
				// [3, 6, 0, 0, 0, 0, 0, 0, 4]
				expect(sudoku.checkBlocFor(3, 0, 1)).to.be.true;
				expect(sudoku.checkBlocFor(3, 0, 2)).to.be.true;
				expect(sudoku.checkBlocFor(3, 0, 3)).to.be.false;
				expect(sudoku.checkBlocFor(3, 0, 4)).to.be.false;
				expect(sudoku.checkBlocFor(3, 0, 5)).to.be.true;
				expect(sudoku.checkBlocFor(3, 0, 6)).to.be.false;
				expect(sudoku.checkBlocFor(3, 0, 7)).to.be.true;
				expect(sudoku.checkBlocFor(3, 0, 8)).to.be.true;
				expect(sudoku.checkBlocFor(3, 0, 9)).to.be.true;
	
				// [0, 0, 1, 2, 0, 0, 0, 0, 3]
				expect(sudoku.checkBlocFor(3, 3, 1)).to.be.false;
				expect(sudoku.checkBlocFor(3, 3, 2)).to.be.false;
				expect(sudoku.checkBlocFor(3, 3, 3)).to.be.false;
				expect(sudoku.checkBlocFor(3, 3, 4)).to.be.true;
				expect(sudoku.checkBlocFor(3, 3, 5)).to.be.true;
				expect(sudoku.checkBlocFor(3, 3, 6)).to.be.true;
				expect(sudoku.checkBlocFor(3, 3, 7)).to.be.true;
				expect(sudoku.checkBlocFor(3, 3, 8)).to.be.true;
				expect(sudoku.checkBlocFor(3, 3, 9)).to.be.true;
	
				// [4, 2, 0, 5, 3, 0, 0, 6, 7]
				expect(sudoku.checkBlocFor(3, 6, 1)).to.be.true;
				expect(sudoku.checkBlocFor(3, 6, 2)).to.be.false;
				expect(sudoku.checkBlocFor(3, 6, 3)).to.be.false;
				expect(sudoku.checkBlocFor(3, 6, 4)).to.be.false;
				expect(sudoku.checkBlocFor(3, 6, 5)).to.be.false;
				expect(sudoku.checkBlocFor(3, 6, 6)).to.be.false;
				expect(sudoku.checkBlocFor(3, 6, 7)).to.be.false;
				expect(sudoku.checkBlocFor(3, 6, 8)).to.be.true;
				expect(sudoku.checkBlocFor(3, 6, 9)).to.be.true;
	
				// [7, 0, 0, 0, 1, 0, 6, 3, 0]
				expect(sudoku.checkBlocFor(6, 0, 1)).to.be.false;
				expect(sudoku.checkBlocFor(6, 0, 2)).to.be.true;
				expect(sudoku.checkBlocFor(6, 0, 3)).to.be.false;
				expect(sudoku.checkBlocFor(6, 0, 4)).to.be.true;
				expect(sudoku.checkBlocFor(6, 0, 5)).to.be.true;
				expect(sudoku.checkBlocFor(6, 0, 6)).to.be.false;
				expect(sudoku.checkBlocFor(6, 0, 7)).to.be.false;
				expect(sudoku.checkBlocFor(6, 0, 8)).to.be.true;
				expect(sudoku.checkBlocFor(6, 0, 9)).to.be.true;
	
				// [0, 0, 8, 3, 0, 6, 0, 0, 0]
				expect(sudoku.checkBlocFor(6, 3, 1)).to.be.true;
				expect(sudoku.checkBlocFor(6, 3, 2)).to.be.true;
				expect(sudoku.checkBlocFor(6, 3, 3)).to.be.false;
				expect(sudoku.checkBlocFor(6, 3, 4)).to.be.true;
				expect(sudoku.checkBlocFor(6, 3, 5)).to.be.true;
				expect(sudoku.checkBlocFor(6, 3, 6)).to.be.false;
				expect(sudoku.checkBlocFor(6, 3, 7)).to.be.true;
				expect(sudoku.checkBlocFor(6, 3, 8)).to.be.false;
				expect(sudoku.checkBlocFor(6, 3, 9)).to.be.true;
	
				// [0, 0, 0, 0, 5, 0, 0, 4, 0]
				expect(sudoku.checkBlocFor(6, 6, 1)).to.be.true;
				expect(sudoku.checkBlocFor(6, 6, 2)).to.be.true;
				expect(sudoku.checkBlocFor(6, 6, 3)).to.be.true;
				expect(sudoku.checkBlocFor(6, 6, 4)).to.be.false;
				expect(sudoku.checkBlocFor(6, 6, 5)).to.be.false;
				expect(sudoku.checkBlocFor(6, 6, 6)).to.be.true;
				expect(sudoku.checkBlocFor(6, 6, 7)).to.be.true;
				expect(sudoku.checkBlocFor(6, 6, 8)).to.be.true;
				expect(sudoku.checkBlocFor(6, 6, 9)).to.be.true;
			});
		});

		describe('getPossibleNumbersFor', () => {
			it('Expect to find all possibles number for the coordinates x/y', () => {
				const sudoku = new Sudoku(source);
	
				expect(sudoku.getPossibleNumbersFor(0, 0)).to.deep.equal([1, 2, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(0, 1)).to.deep.equal([2, 5, 7, 9]);
				expect(sudoku.getPossibleNumbersFor(0, 2)).to.deep.equal([1, 2, 3, 5, 6, 7, 9]);
				expect(sudoku.getPossibleNumbersFor(0, 3)).to.deep.equal([1, 5, 6, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(0, 4)).to.deep.equal([1, 2, 3, 5, 6, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(0, 5)).to.deep.equal([2, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(0, 6)).to.deep.equal([1, 2, 3, 7, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(0, 7)).to.deep.equal([1, 7, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(0, 8)).to.deep.equal([1, 2, 3, 5, 8, 9]);
	
				expect(sudoku.getPossibleNumbersFor(1, 0)).to.deep.equal([1, 2, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(1, 1)).to.deep.equal([2, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(1, 2)).to.deep.equal([1, 2, 3, 5, 6, 9]);
				expect(sudoku.getPossibleNumbersFor(1, 3)).to.deep.equal([1, 4, 5, 6, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(1, 4)).to.deep.equal([1, 2, 3, 4, 5, 6, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(1, 5)).to.deep.equal([2, 4, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(1, 6)).to.deep.equal([1, 2, 3, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(1, 7)).to.deep.equal([1, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(1, 8)).to.deep.equal([1, 2, 3, 4, 5, 8, 9]);
	
				expect(sudoku.getPossibleNumbersFor(2, 0)).to.deep.equal([1, 2, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(2, 1)).to.deep.equal([2, 5, 7, 9]);
				expect(sudoku.getPossibleNumbersFor(2, 2)).to.deep.equal([1, 2, 3, 5, 7,9]);
				expect(sudoku.getPossibleNumbersFor(2, 3)).to.deep.equal([1, 4, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(2, 4)).to.deep.equal([1, 2, 3, 4, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(2, 5)).to.deep.equal([2, 4, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(2, 6)).to.deep.equal([1, 2, 3, 7, 9]);
				expect(sudoku.getPossibleNumbersFor(2, 7)).to.deep.equal([1, 7, 9]);
				expect(sudoku.getPossibleNumbersFor(2, 8)).to.deep.equal([1, 2, 3, 4, 5, 9]);
	
				expect(sudoku.getPossibleNumbersFor(3, 0)).to.deep.equal([5, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(3, 1)).to.deep.equal([5, 7, 9]);
				expect(sudoku.getPossibleNumbersFor(3, 2)).to.deep.equal([5, 7, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(3, 3)).to.deep.equal([5, 7, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(3, 4)).to.deep.equal([5, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(3, 5)).to.deep.equal([5, 7, 9]);
				expect(sudoku.getPossibleNumbersFor(3, 6)).to.deep.equal([8, 9]);
				expect(sudoku.getPossibleNumbersFor(3, 7)).to.deep.equal([8, 9]);
				expect(sudoku.getPossibleNumbersFor(3, 8)).to.deep.equal([8, 9]);
				
				expect(sudoku.getPossibleNumbersFor(4, 0)).to.deep.equal([1, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(4, 1)).to.deep.equal([7, 9]);
				expect(sudoku.getPossibleNumbersFor(4, 2)).to.deep.equal([1, 7, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(4, 3)).to.deep.equal([4, 6, 7, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(4, 4)).to.deep.equal([4, 6, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(4, 5)).to.deep.equal([4, 7, 9]);
				expect(sudoku.getPossibleNumbersFor(4, 6)).to.deep.equal([1, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(4, 7)).to.deep.equal([1, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(4, 8)).to.deep.equal([1, 8, 9]);
	
				expect(sudoku.getPossibleNumbersFor(5, 0)).to.deep.equal([1, 2, 5, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(5, 1)).to.deep.equal([2, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(5, 2)).to.deep.equal([1, 2, 5, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(5, 3)).to.deep.equal([5, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(5, 4)).to.deep.equal([5, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(5, 5)).to.deep.equal([5, 9]);
				expect(sudoku.getPossibleNumbersFor(5, 6)).to.deep.equal([1, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(5, 7)).to.deep.equal([1, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(5, 8)).to.deep.equal([1, 8, 9]);
	
				expect(sudoku.getPossibleNumbersFor(6, 0)).to.deep.equal([2, 4, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(6, 1)).to.deep.equal([2, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(6, 2)).to.deep.equal([2, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(6, 3)).to.deep.equal([1, 4, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(6, 4)).to.deep.equal([1, 2, 4, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(6, 5)).to.deep.equal([2, 4, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(6, 6)).to.deep.equal([1, 2, 3, 9]);
				expect(sudoku.getPossibleNumbersFor(6, 7)).to.deep.equal([1, 9]);
				expect(sudoku.getPossibleNumbersFor(6, 8)).to.deep.equal([1, 2, 3, 6, 9]);
	
				expect(sudoku.getPossibleNumbersFor(7, 0)).to.deep.equal([2, 4, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(7, 1)).to.deep.equal([2, 9]);
				expect(sudoku.getPossibleNumbersFor(7, 2)).to.deep.equal([2, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(7, 3)).to.deep.equal([4, 7, 9]);
				expect(sudoku.getPossibleNumbersFor(7, 4)).to.deep.equal([2, 4, 9]);
				expect(sudoku.getPossibleNumbersFor(7, 5)).to.deep.equal([2, 4, 7, 9]);
				expect(sudoku.getPossibleNumbersFor(7, 6)).to.deep.equal([2, 7, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(7, 7)).to.deep.equal([7, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(7, 8)).to.deep.equal([2, 8, 9]);
	
				expect(sudoku.getPossibleNumbersFor(8, 0)).to.deep.equal([2, 5, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(8, 1)).to.deep.equal([2, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(8, 2)).to.deep.equal([2, 5, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(8, 3)).to.deep.equal([1, 5, 7, 9]);
				expect(sudoku.getPossibleNumbersFor(8, 4)).to.deep.equal([1, 2, 5, 9]);
				expect(sudoku.getPossibleNumbersFor(8, 5)).to.deep.equal([2, 5, 7, 9]);
				expect(sudoku.getPossibleNumbersFor(8, 6)).to.deep.equal([1, 2, 7, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(8, 7)).to.deep.equal([1, 7, 8, 9]);
				expect(sudoku.getPossibleNumbersFor(8, 8)).to.deep.equal([1, 2, 8, 9]);
			});
		});
	
		describe('isComplete', () => {
			it('Expect to return false if the sudoku is not ended', () => {
				let sudoku = new Sudoku(source);
	
				expect(sudoku.isComplete()).to.be.false;
			});
	
			it('Expect to return true if all cases of the sudoku has been filled', () => {
				let sudoku = new Sudoku([
					[1, 2, 3, 4, 5, 6, 7, 8, 9],
					[2, 3, 4, 5, 6, 7, 8, 9, 1],
					[3, 4, 5, 6, 7, 8, 9, 1, 2],
					[4, 5, 6, 7, 8, 9, 1, 2, 3],
					[5, 6, 7, 8, 9, 1, 2, 3, 4],
					[6, 7, 8, 9, 1, 2, 3, 4, 5],
					[7, 8, 9, 1, 2, 3, 4, 5, 6],
					[8, 9, 1, 2, 3, 4, 5, 6, 7],
					[9, 1, 2, 3, 4, 5, 6, 7, 8]
				]);
	
				expect(sudoku.isComplete()).to.be.true;
			});
		});
	
		describe('getLine', () => {
			it('Expect to return the line x', () => {
				let sudoku = new Sudoku(source);
	
				expect(sudoku.getLine(0)).to.deep.equal([0, 4, 0,	0, 0, 0, 	0, 0, 0]);
				expect(sudoku.getLine(1)).to.deep.equal([0, 0, 0, 	0, 7, 0, 	0, 0, 0]);
				expect(sudoku.getLine(2)).to.deep.equal([0, 8, 0, 	0, 0, 0, 	6, 0, 0]);
				expect(sudoku.getLine(3)).to.deep.equal([3, 6, 0, 	0, 0, 1, 	4, 2, 0]);
				expect(sudoku.getLine(4)).to.deep.equal([0, 0, 0, 	2, 0, 0, 	5, 3, 0]);
				expect(sudoku.getLine(5)).to.deep.equal([0, 0, 4, 	0, 0, 3, 	0, 6, 7]);
				expect(sudoku.getLine(6)).to.deep.equal([7, 0, 0, 	0, 0, 8, 	0, 0, 0]);
				expect(sudoku.getLine(7)).to.deep.equal([0, 1, 0, 	3, 0, 6, 	0, 5, 0]);
				expect(sudoku.getLine(8)).to.deep.equal([6, 3, 0, 	0, 0, 0, 	0, 4, 0]);
			});
		});
	
		describe('getColumn', () => {
			it('Expect to return the column y', () => {
				let sudoku = new Sudoku(source);
	
				expect(sudoku.getColumn(0)).to.deep.equal([0, 0, 0, 3, 0, 0, 7, 0, 6]);
				expect(sudoku.getColumn(1)).to.deep.equal([4, 0, 8, 6, 0, 0, 0, 1, 3]);
				expect(sudoku.getColumn(2)).to.deep.equal([0, 0, 0, 0, 0, 4, 0, 0, 0]);
				expect(sudoku.getColumn(3)).to.deep.equal([0, 0, 0, 0, 2, 0, 0, 3, 0]);
				expect(sudoku.getColumn(4)).to.deep.equal([0, 7, 0, 0, 0, 0, 0, 0, 0]); 
				expect(sudoku.getColumn(5)).to.deep.equal([0, 0, 0, 1, 0, 3, 8, 6, 0]); 
				expect(sudoku.getColumn(6)).to.deep.equal([0, 0, 6, 4, 5, 0, 0, 0, 0]); 
				expect(sudoku.getColumn(7)).to.deep.equal([0, 0, 0, 2, 3, 6, 0, 5, 4]); 
				expect(sudoku.getColumn(8)).to.deep.equal([0, 0, 0, 0, 0, 7, 0, 0, 0]);
			});
		});
	
		describe('getBlock', () => {
			it('Expect to return the block containing the given x/y', () => {
				let sudoku = new Sudoku(source);
	
				expect(sudoku.getBlock(0, 0)).to.deep.equal([0, 4, 0, 0, 0, 0, 0, 8, 0]);
				expect(sudoku.getBlock(0, 3)).to.deep.equal([0, 0, 0, 0, 7, 0, 0, 0, 0]);
				expect(sudoku.getBlock(0, 6)).to.deep.equal([0, 0, 0, 0, 0, 0, 6, 0, 0]);
				expect(sudoku.getBlock(3, 0)).to.deep.equal([3, 6, 0, 0, 0, 0, 0, 0, 4]);
				expect(sudoku.getBlock(3, 3)).to.deep.equal([0, 0, 1, 2, 0, 0, 0, 0, 3]);
				expect(sudoku.getBlock(3, 6)).to.deep.equal([4, 2, 0, 5, 3, 0, 0, 6, 7]);
				expect(sudoku.getBlock(6, 0)).to.deep.equal([7, 0, 0, 0, 1, 0, 6, 3, 0]);
				expect(sudoku.getBlock(6, 3)).to.deep.equal([0, 0, 8, 3, 0, 6, 0, 0, 0]);
				expect(sudoku.getBlock(6, 6)).to.deep.equal([0, 0, 0, 0, 5, 0, 0, 4, 0]);
			});
		});
	});
	
	describe('Sudoku.', () => {
		describe('getCoordinatesFromBlock', () => {
			it('Expect to return the right coordinates', () => {
				expect(Sudoku.getCoordinatesFromBlock(0, 0, 0)).to.deep.equal({x: 0, y: 0});
				expect(Sudoku.getCoordinatesFromBlock(0, 0, 1)).to.deep.equal({x: 0, y: 1});
				expect(Sudoku.getCoordinatesFromBlock(0, 0, 2)).to.deep.equal({x: 0, y: 2});
				expect(Sudoku.getCoordinatesFromBlock(0, 0, 3)).to.deep.equal({x: 1, y: 0});
				expect(Sudoku.getCoordinatesFromBlock(0, 0, 4)).to.deep.equal({x: 1, y: 1});
				expect(Sudoku.getCoordinatesFromBlock(0, 0, 5)).to.deep.equal({x: 1, y: 2});
				expect(Sudoku.getCoordinatesFromBlock(0, 0, 6)).to.deep.equal({x: 2, y: 0});
				expect(Sudoku.getCoordinatesFromBlock(0, 0, 7)).to.deep.equal({x: 2, y: 1});
				expect(Sudoku.getCoordinatesFromBlock(0, 0, 8)).to.deep.equal({x: 2, y: 2});
	
				expect(Sudoku.getCoordinatesFromBlock(0, 3, 0)).to.deep.equal({x: 0, y: 3 + 0});
				expect(Sudoku.getCoordinatesFromBlock(0, 3, 1)).to.deep.equal({x: 0, y: 3 + 1});
				expect(Sudoku.getCoordinatesFromBlock(0, 3, 2)).to.deep.equal({x: 0, y: 3 + 2});
				expect(Sudoku.getCoordinatesFromBlock(0, 3, 3)).to.deep.equal({x: 1, y: 3 + 0});
				expect(Sudoku.getCoordinatesFromBlock(0, 3, 4)).to.deep.equal({x: 1, y: 3 + 1});
				expect(Sudoku.getCoordinatesFromBlock(0, 3, 5)).to.deep.equal({x: 1, y: 3 + 2});
				expect(Sudoku.getCoordinatesFromBlock(0, 3, 6)).to.deep.equal({x: 2, y: 3 + 0});
				expect(Sudoku.getCoordinatesFromBlock(0, 3, 7)).to.deep.equal({x: 2, y: 3 + 1});
				expect(Sudoku.getCoordinatesFromBlock(0, 3, 8)).to.deep.equal({x: 2, y: 3 + 2});
	
				expect(Sudoku.getCoordinatesFromBlock(0, 6, 0)).to.deep.equal({x: 0, y: 6 + 0});
				expect(Sudoku.getCoordinatesFromBlock(0, 6, 1)).to.deep.equal({x: 0, y: 6 + 1});
				expect(Sudoku.getCoordinatesFromBlock(0, 6, 2)).to.deep.equal({x: 0, y: 6 + 2});
				expect(Sudoku.getCoordinatesFromBlock(0, 6, 3)).to.deep.equal({x: 1, y: 6 + 0});
				expect(Sudoku.getCoordinatesFromBlock(0, 6, 4)).to.deep.equal({x: 1, y: 6 + 1});
				expect(Sudoku.getCoordinatesFromBlock(0, 6, 5)).to.deep.equal({x: 1, y: 6 + 2});
				expect(Sudoku.getCoordinatesFromBlock(0, 6, 6)).to.deep.equal({x: 2, y: 6 + 0});
				expect(Sudoku.getCoordinatesFromBlock(0, 6, 7)).to.deep.equal({x: 2, y: 6 + 1});
				expect(Sudoku.getCoordinatesFromBlock(0, 6, 8)).to.deep.equal({x: 2, y: 6 + 2});
	
				expect(Sudoku.getCoordinatesFromBlock(3, 0, 0)).to.deep.equal({x: 3 + 0, y: 0});
				expect(Sudoku.getCoordinatesFromBlock(3, 0, 1)).to.deep.equal({x: 3 + 0, y: 1});
				expect(Sudoku.getCoordinatesFromBlock(3, 0, 2)).to.deep.equal({x: 3 + 0, y: 2});
				expect(Sudoku.getCoordinatesFromBlock(3, 0, 3)).to.deep.equal({x: 3 + 1, y: 0});
				expect(Sudoku.getCoordinatesFromBlock(3, 0, 4)).to.deep.equal({x: 3 + 1, y: 1});
				expect(Sudoku.getCoordinatesFromBlock(3, 0, 5)).to.deep.equal({x: 3 + 1, y: 2});
				expect(Sudoku.getCoordinatesFromBlock(3, 0, 6)).to.deep.equal({x: 3 + 2, y: 0});
				expect(Sudoku.getCoordinatesFromBlock(3, 0, 7)).to.deep.equal({x: 3 + 2, y: 1});
				expect(Sudoku.getCoordinatesFromBlock(3, 0, 8)).to.deep.equal({x: 3 + 2, y: 2});
	
				expect(Sudoku.getCoordinatesFromBlock(3, 3, 0)).to.deep.equal({x: 3 + 0, y: 3 + 0});
				expect(Sudoku.getCoordinatesFromBlock(3, 3, 1)).to.deep.equal({x: 3 + 0, y: 3 + 1});
				expect(Sudoku.getCoordinatesFromBlock(3, 3, 2)).to.deep.equal({x: 3 + 0, y: 3 + 2});
				expect(Sudoku.getCoordinatesFromBlock(3, 3, 3)).to.deep.equal({x: 3 + 1, y: 3 + 0});
				expect(Sudoku.getCoordinatesFromBlock(3, 3, 4)).to.deep.equal({x: 3 + 1, y: 3 + 1});
				expect(Sudoku.getCoordinatesFromBlock(3, 3, 5)).to.deep.equal({x: 3 + 1, y: 3 + 2});
				expect(Sudoku.getCoordinatesFromBlock(3, 3, 6)).to.deep.equal({x: 3 + 2, y: 3 + 0});
				expect(Sudoku.getCoordinatesFromBlock(3, 3, 7)).to.deep.equal({x: 3 + 2, y: 3 + 1});
				expect(Sudoku.getCoordinatesFromBlock(3, 3, 8)).to.deep.equal({x: 3 + 2, y: 3 + 2});
	
				expect(Sudoku.getCoordinatesFromBlock(3, 6, 0)).to.deep.equal({x: 3 + 0, y: 6 + 0});
				expect(Sudoku.getCoordinatesFromBlock(3, 6, 1)).to.deep.equal({x: 3 + 0, y: 6 + 1});
				expect(Sudoku.getCoordinatesFromBlock(3, 6, 2)).to.deep.equal({x: 3 + 0, y: 6 + 2});
				expect(Sudoku.getCoordinatesFromBlock(3, 6, 3)).to.deep.equal({x: 3 + 1, y: 6 + 0});
				expect(Sudoku.getCoordinatesFromBlock(3, 6, 4)).to.deep.equal({x: 3 + 1, y: 6 + 1});
				expect(Sudoku.getCoordinatesFromBlock(3, 6, 5)).to.deep.equal({x: 3 + 1, y: 6 + 2});
				expect(Sudoku.getCoordinatesFromBlock(3, 6, 6)).to.deep.equal({x: 3 + 2, y: 6 + 0});
				expect(Sudoku.getCoordinatesFromBlock(3, 6, 7)).to.deep.equal({x: 3 + 2, y: 6 + 1});
				expect(Sudoku.getCoordinatesFromBlock(3, 6, 8)).to.deep.equal({x: 3 + 2, y: 6 + 2});
	
				expect(Sudoku.getCoordinatesFromBlock(6, 0, 0)).to.deep.equal({x: 6 + 0, y: 0});
				expect(Sudoku.getCoordinatesFromBlock(6, 0, 1)).to.deep.equal({x: 6 + 0, y: 1});
				expect(Sudoku.getCoordinatesFromBlock(6, 0, 2)).to.deep.equal({x: 6 + 0, y: 2});
				expect(Sudoku.getCoordinatesFromBlock(6, 0, 3)).to.deep.equal({x: 6 + 1, y: 0});
				expect(Sudoku.getCoordinatesFromBlock(6, 0, 4)).to.deep.equal({x: 6 + 1, y: 1});
				expect(Sudoku.getCoordinatesFromBlock(6, 0, 5)).to.deep.equal({x: 6 + 1, y: 2});
				expect(Sudoku.getCoordinatesFromBlock(6, 0, 6)).to.deep.equal({x: 6 + 2, y: 0});
				expect(Sudoku.getCoordinatesFromBlock(6, 0, 7)).to.deep.equal({x: 6 + 2, y: 1});
				expect(Sudoku.getCoordinatesFromBlock(6, 0, 8)).to.deep.equal({x: 6 + 2, y: 2});
	
				expect(Sudoku.getCoordinatesFromBlock(6, 3, 0)).to.deep.equal({x: 6 + 0, y: 3 + 0});
				expect(Sudoku.getCoordinatesFromBlock(6, 3, 1)).to.deep.equal({x: 6 + 0, y: 3 + 1});
				expect(Sudoku.getCoordinatesFromBlock(6, 3, 2)).to.deep.equal({x: 6 + 0, y: 3 + 2});
				expect(Sudoku.getCoordinatesFromBlock(6, 3, 3)).to.deep.equal({x: 6 + 1, y: 3 + 0});
				expect(Sudoku.getCoordinatesFromBlock(6, 3, 4)).to.deep.equal({x: 6 + 1, y: 3 + 1});
				expect(Sudoku.getCoordinatesFromBlock(6, 3, 5)).to.deep.equal({x: 6 + 1, y: 3 + 2});
				expect(Sudoku.getCoordinatesFromBlock(6, 3, 6)).to.deep.equal({x: 6 + 2, y: 3 + 0});
				expect(Sudoku.getCoordinatesFromBlock(6, 3, 7)).to.deep.equal({x: 6 + 2, y: 3 + 1});
				expect(Sudoku.getCoordinatesFromBlock(6, 3, 8)).to.deep.equal({x: 6 + 2, y: 3 + 2});
	
				expect(Sudoku.getCoordinatesFromBlock(6, 6, 0)).to.deep.equal({x: 6 + 0, y: 6 + 0});
				expect(Sudoku.getCoordinatesFromBlock(6, 6, 1)).to.deep.equal({x: 6 + 0, y: 6 + 1});
				expect(Sudoku.getCoordinatesFromBlock(6, 6, 2)).to.deep.equal({x: 6 + 0, y: 6 + 2});
				expect(Sudoku.getCoordinatesFromBlock(6, 6, 3)).to.deep.equal({x: 6 + 1, y: 6 + 0});
				expect(Sudoku.getCoordinatesFromBlock(6, 6, 4)).to.deep.equal({x: 6 + 1, y: 6 + 1});
				expect(Sudoku.getCoordinatesFromBlock(6, 6, 5)).to.deep.equal({x: 6 + 1, y: 6 + 2});
				expect(Sudoku.getCoordinatesFromBlock(6, 6, 6)).to.deep.equal({x: 6 + 2, y: 6 + 0});
				expect(Sudoku.getCoordinatesFromBlock(6, 6, 7)).to.deep.equal({x: 6 + 2, y: 6 + 1});
				expect(Sudoku.getCoordinatesFromBlock(6, 6, 8)).to.deep.equal({x: 6 + 2, y: 6 + 2});
			});
		});

		describe('checkForN', () => {
			it('Expect to return if there is no n', () => {
				expect(Sudoku.checkForN([9, 2, 3], 1)).to.be.true;
				expect(Sudoku.checkForN([[1], [2], [3]], 1)).to.be.true;
				expect(Sudoku.checkForN([1, 2, 3], [1, 2, 3], [2, 3], 1)).to.be.true;
				expect(Sudoku.checkForN([1, 2, 3], 1)).to.be.false;
				expect(Sudoku.checkForN([1, [2], [3]], 1)).to.be.false;
				expect(Sudoku.checkForN([1, [2, 3], [2, 3]], 1)).to.be.false;
			});
		});
	
		describe('findOnlyHint', () => {
			it('Expect to return the index of where there is only hint', () => {
				expect(Sudoku.findOnlyHint([[1], 2, 3])).to.equal(0);
				expect(Sudoku.findOnlyHint([[1], [2], [3]])).to.equal(0);
			});
	
			it('Expect to return -1 if there is no only hint', () => {
				expect(Sudoku.findOnlyHint([[], 2, 3])).to.equal(-1);
				expect(Sudoku.findOnlyHint([[], [2, 3], [2, 3]])).to.equal(-1);
			});
		});

		describe('findOnlyPossibleN', () => {
			it('Expect to return the index and number of the only missing number', () => {
				expect(Sudoku.findOnlyPossibleN([1, 2, 3, 0, 5, 6, 7, 8, 9])).to.deep.equal({i: 3, n: 4});
				expect(Sudoku.findOnlyPossibleN([1, 2, 3, 4, [], 6, 7, 8, 9])).to.deep.equal({i: 4, n: 5});
				expect(Sudoku.findOnlyPossibleN([1, 2, 3, 4, 5, 6, 7, 8, 9])).to.deep.equal({i: -1, n: null});
			});
		});
	});
});