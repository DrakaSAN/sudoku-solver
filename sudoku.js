const Logger = require('blad-logger'),
	log = new Logger('sudoku'),
	fs = require('fs'),
	ejs = require('ejs');

Logger.configure({
	console: {
		level: 'debug'
		// level: 'silly'
	}
});

/* 
	Source:
		Y
		0 1 2 3 4 5 6 7 8
	X	0
		1
		2
		3
		4
		5
		6
		7
		8
	
	/!\ MUST BE SQUARE
*/

let numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9];

const Sudoku = class Sudoku {
	constructor(s) {
		if(s.length !== numbers.length || !s.every((s) => {return s.length === numbers.length;})) {
			throw new Error('Size is wrong, expected a ' + numbers.length + ' / ' + numbers.length + ' board');
		}
		this.playboard = s;
		this.log = [];
	}

	// Initialize the hints
	// To be called before run
	init() {
		this.setHint();
		this.print();
	}

	// Complete the sudoku
	// Return false if it got stuck
	run() {
		log.debug('run');
		let step = 0;
		// this.print();

		while(!this.isComplete()) {
			step = this.log.length;
			try {
				this.step();
				if(step === this.log.length) {
					throw new Error('Stuck');
				}
			} catch(error) {
				this.print(error.message);
				throw error;
			}
		}

		this.print();
		log.info('Completed in ' + this.log.length + ' step');
	}

	// Search for a possible change, apply it, and return if a change has been made
	step() {
		// Try every function one after another
		log.debug('step');

		this.setHint();
		this.setOnlyHint();
		this.setUniqueHint();
		this.setOnlyPossibleN();
	}

	print(error) {
		log.info('Current state', {state: this.playboard});
		let html = ejs.render(
			fs.readFileSync('./views/template.ejs', 'utf8'), 
			{
				sudoku: this.playboard, 
				log: this.log,
				error
			}
		);
		fs.writeFileSync('./out/last.html', html);
	}

	set(x, y, n) {
		if(typeof x === 'object') {
			y = x.y;
			n = x.n;
			x = x.x;
		}
		let old = this.playboard[x][y];
		// this.print();
		if(numbers.includes(this.playboard[x][y])) {
			throw new Error('Trying to change already defined number');
		}
		if(!this.checkCaseFor(x, y, n)) {
			throw new Error('Invalid move ' + x + ' / ' + y + ' => ' + n);
		}
		log.debug('set', {x, y, old, new: n});

		this.log.push({x, y, old, new: n});
		this.playboard[x][y] = n;

		// If a change has been made, update the hints
		if(!Array.isArray(n)) {
			this.setHint();
		}
	}

	isComplete() {
		return this.playboard.every((line) => {
			return line.every((e) => {
				return numbers.includes(e);
			});
		});
	}

	getLine(x) {
		return this.playboard[x];
	}

	getColumn(y) {
		return this.playboard.map((line) => {return line[y];});
	}

	getBlock(xi, yi) {
		let bc = getBlockWithCoordinates(xi, yi),
			c = getCoordinatesOfBlock(bc.x, bc.y),
			xb = c.x,
			yb = c.y,
			block = [];

		xb.map((x) => {
			return yb.map((y) => {
				block.push(this.playboard[x][y]);
			});
		});
		return block;
	}

	// Return true if the line x doesn't have any n
	checkLineFor(x, n) {
		return checkForN(this.getLine(x), n);
	}
	
	// Return true if the column y doesn't have any n
	checkColumnFor(y, n) {
		return checkForN(this.getColumn(y), n);
	}
	
	// Return true if the bloc containing the case x y doesn't have any n
	checkBlocFor(x, y, n) {
		return checkForN(this.getBlock(x, y), n);
	}
	
	checkCaseFor(x, y, n) {
		return (
			this.checkLineFor(x, n) &&
			this.checkColumnFor(y, n) &&
			this.checkBlocFor(x, y, n)
		);
	}

	iterateLineColumnBlock(funcLine, funcColumn, funcBlock) {
		let x = 0,
			maxX = this.playboard.length,
			y = 0,
			maxY = this.playboard[0].length;

		// Lines
		while(x < maxX) {
			this[funcLine](x);
			x = x + 1;
		}
		// Column
		while(y < maxY) {
			this[funcColumn](y);
			y = y + 1;
		}
		// Block
		x = 0;
		y = 0;
		while(x < maxX) {
			y = 0;
			while(y < maxY) {
				this[funcBlock](x, y);
				y = y + 3;
			}
			x = x + 3;
		}
	}

	// Search and set all only hint in the line
	setOnlyHintInLine(x) {
		let step = null;

		// While there is change mades
		while(step !== this.log.length) {
			step = this.log.length;
			// Try to find a only hint
			let line = this.getLine(x),
				y = findOnlyHint(line);
			// If one is found, change it for the number
			if(y !== -1) {
				this.set(x, y, this.playboard[x][y][0]);
			}
		}
	}
	
	// Search and set all only hint in the column
	setOnlyHintInColumn(y) {
		let step = null;

		// While there is change mades
		while(step !== this.log.length) {
			step = this.log.length;
			// Try to find a only hint
			let column = this.getColumn(y),
				x = findOnlyHint(column);
			// If one is found, change it for the number
			if(x !== -1) {
				this.set(x, y, this.playboard[x][y][0]);
			}
		}
	}
	
	// Search and set all only hint in the column
	setOnlyHintInBlock(x, y) {
		let step = null;

		// While there is change mades
		while(step !== this.log.length) {
			step = this.log.length;
			// Try to find a only hint
			let block = this.getBlock(x, y),
				i = findOnlyHint(block);
			// If one is found, change it for the number
			if(i !== -1) {
				let c = this.getCoordinatesFromBlock(x, y, i);
				this.set(c.x, c.y, this.playboard[c.x][c.y][0]);
			}
		}
	}
	
	setOnlyHint() {
		log.debug('setOnlyHint');
		this.iterateLineColumnBlock(
			'setOnlyHintInLine',
			'setOnlyHintInColumn',
			'setOnlyHintInBlock'
		);
	}

	// Search and set all only hint in the line
	setUniqueHintInLine(x) {
		let step = null;

		// While there is change mades
		while(step !== this.log.length) {
			step = this.log.length;
			// Try to find a only hint
			let line = this.getLine(x),
				c = findUniqueHint(line);
			// If one is found, change it for the number
			if(c.i !== -1 && c.n !== null) {
				this.set(x, c.i, c.n);
			}
		}
	}
	
	// Search and set all only hint in the column
	setUniqueHintInColumn(y) {
		let step = null;

		// While there is change mades
		while(step !== this.log.length) {
			step = this.log.length;
			// Try to find a only hint
			let column = this.getColumn(y),
				c = findUniqueHint(column);
			// If one is found, change it for the number
			if(c.i !== -1 && c.n !== null) {
				this.set(c.i, y, c.n);
			}
		}
	}
	
	// Search and set all only hint in the column
	setUniqueHintInBlock(x, y) {
		let step = null;

		// While there is change mades
		while(step !== this.log.length) {
			step = this.log.length;
			// Try to find a only hint
			let block = this.getBlock(x, y),
				c = findUniqueHint(block);
			// If one is found, change it for the number
			if(c.i !== -1) {
				let d = getCoordinatesFromBlock(x, y, c.i);
				this.set(d.x, d.y, c.n);
			}
		}
	}
	
	setUniqueHint() {
		log.debug('setUniqueHint');
		this.iterateLineColumnBlock(
			'setUniqueHintInLine',
			'setUniqueHintInColumn',
			'setUniqueHintInBlock'
		);
	}

	setOnlyPossibleNInLine(x) {
		let step = null;
		
		// While there is change mades
		while(step !== this.log.length) {
			let c = findOnlyPossibleN(this.getLine(x));
			if(c.i != -1 && c.n !== null) {
				this.set(x, c.i, c.n);
			}
			step = this.log.length;
		}
	}

	setOnlyPossibleNInColumn(y) {
		let step = null;
		
		// While there is change mades
		while(step !== this.log.length) {
			let c = findOnlyPossibleN(this.getColumn(y));
			if(c.i != -1 && c.n !== null) {
				this.set(c.i, y, c.n);
			}
			step = this.log.length;
		}
	}

	setOnlyPossibleNInBlock(x, y) {
		let step = null;
		
		// While there is change mades
		while(step !== this.log.length) {
			let c = findOnlyPossibleN(this.getLine(x));
			if(c.i != -1 && c.n !== null) {
				let d = this.getCoordinatesFromBlock(x, y, c.i);
				
				this.set(d.x, d.y, c.n);
			}
			step = this.log.length;
		}
	}

	setOnlyPossibleN() {
		log.debug('setOnlyPossibleN');
		this.iterateLineColumnBlock(
			'setOnlyPossibleNInLine',
			'setOnlyPossibleNInColumn',
			'setOnlyPossibleNInBlock'
		);
	}

	// Return all possible number in that case
	getPossibleNumbersFor(x, y) {
		// log.debug('getPossibleNumbersFor', {x, y});
		return numbers.filter((n) => {
			return (
				this.checkBlocFor(x, y, n) &&
				this.checkLineFor(x, n) &&
				this.checkColumnFor(y, n)
			);
		});
	}

	// Iterate the cases of the sudoku and call the function on it
	// The function receive must have a signature of (element, x, y)
	applyToSudoku(func) {
		log.debug('applyToSudoku');

		let x = 0,
			y = 0;
		
		while(x < this.playboard.length) {
			y = 0;
			while(y < this.playboard[x].length) {
				func(this.playboard[x][y], x, y);
				y = y + 1;
			}
			x = x + 1;
		}
	}
	
	// Update hints
	setHint() {
		log.debug('setHint');

		return this.applyToSudoku((e, x, y) => {
			log.silly('setHint', {e, x, y});
			// If the element is already set, no need for hints
			if(numbers.includes(e)) {
				log.silly('setHint is a number');
				return;
			}

			let hints = this.getPossibleNumbersFor(x, y);
			// If hints where already set and are the same as the ones calculated
			if(
				hints.length === this.playboard[x][y].length && 
				hints.every((h, i) => {return h === this.playboard[x][y][i];})
			) {
				log.silly('setHint hints identical', {hints});
				return;
			} else {
				log.silly('setHint new hint', {hints});
				this.set(x, y, hints);
			}
		});
	}
};

function checkForN(source, n) {
	log.silly('checkForN', {source, n});
	return source.every((e) => {
		return (e !== n);
	});
}
Sudoku.checkForN = checkForN;

function getBlockWithCoordinates(xi, yi) {
	log.silly('getBlockWithCoordinates', {xi, yi});
	let c = [[0, 1, 2], [3, 4, 5], [6, 7, 8]],
		xb = c.findIndex((x) => {return x.includes(xi);}),
		yb = c.findIndex((y) => {return y.includes(yi);});

	log.silly('getBlockWithCoordinates', {xb, yb});
	return {x: xb, y: yb};
}
Sudoku.getBlockWithCoordinates = getBlockWithCoordinates;

function getCoordinatesOfBlock(xi, yi) {
	log.silly('getCoordinatesOfBlock', {xi, yi});
	let c = [[0, 1, 2], [3, 4, 5], [6, 7, 8]],
		xb = c[xi],
		yb = c[yi];
	
	if(xi > c.length || yi > c.length) {
		return false;
	}
	
	log.silly('getCoordinatesOfBlock', {xb, yb});
	return {
		x: xb,
		y: yb
	};
}
Sudoku.getCoordinatesOfBlock = getCoordinatesOfBlock;

// Return the index of where in the source there is a hint not repeated
function findOnlyHint(source) {
	log.silly('findOnlyHint', {source});
	return source.findIndex((e) => {
		if(numbers.includes(e)) {
			return false;
		}

		return (e.length === 1);
	});
}
Sudoku.findOnlyHint = findOnlyHint;

function findUniqueHint(source) {
	log.silly('findUniqueHint', {source});

	let pkmn = 0;

	while(pkmn < numbers.length) {
		let n = numbers[pkmn];
		// Count the number of apperance of the hint
		let h = source.reduce((c, e) => {
			return (
				!numbers.includes(e) &&
				e.includes(n)
			)? c + 1:c;
		}, 0);
	
		// If there is only one hint, return its index and the number
		if(h === 1) {
			let i = source.findIndex((e) => {
				return (!numbers.includes(e) && e.includes(n));
			});
			return {
				i,
				n 
			};
		}
		pkmn = pkmn + 1;
	}
	return {
		i: -1,
		n: null
	};
}
Sudoku.findUniqueHint = findUniqueHint;

function findOnlyPossibleN(source) {
	// For each numbers, verify it is included in the source
	let nums = numbers.map((n) => {
		return (source.includes(n))?true:n;
	});
	// If there is only one number missing
	let missing = nums.filter((e) => {return typeof e === 'number';}).length;
	if(missing === 1) {
		// Find the index
		let i = nums.findIndex((e) => {
			return numbers.includes(e);
		});
		// Find the number
		let n = nums[i];
		
		return {n, i};
	} else {
		return {
			i: -1,
			n: null
		};
	}
}
Sudoku.findOnlyPossibleN = findOnlyPossibleN;

function getCoordinatesFromBlock(x, y, i) {
	const o = [
		{x: 0, y: 0}, {x: 0, y: 1}, {x: 0, y: 2},
		{x: 1, y: 0}, {x: 1, y: 1}, {x: 1, y: 2},
		{x: 2, y: 0}, {x: 2, y: 1}, {x: 2, y: 2}
	];
	let r = o[i];
	return {
		x: x + r.x,
		y: y + r.y
	};
}
Sudoku.getCoordinatesFromBlock = getCoordinatesFromBlock;

module.exports = Sudoku;